//
//  PCCart.swift
//  productCartTestAssignment
//
//  Created by Serhii on 4/20/17.
//  Copyright © 2017 Serhii. All rights reserved.
//

import Foundation

final class PCCart: PCListable {
    static var shared = PCCart()
    var productList: [ProductObject]
    var totalPrice: Int {
        var tempTotal = 0
        for product in productList {
            tempTotal += product.quantity * product.price
        }
        return tempTotal
    }
    var totalProducts: Int {
        var tempTotal = 0
        for product in productList {
            tempTotal += product.quantity
        }
        return tempTotal
    }
    
    init(productList: [ProductObject] = [ProductObject]()) {
        self.productList = productList
    }

}
