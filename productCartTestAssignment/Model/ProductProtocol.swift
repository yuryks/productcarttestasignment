//
//  ProductProtocol.swift
//  productCartTestAssignment
//
//  Created by Serhii on 4/19/17.
//  Copyright © 2017 Serhii. All rights reserved.
//

import UIKit

protocol PCProductable {
    var name: String {get set}
    var price: Int {get set}
    var quantity: Int {get set}
}

