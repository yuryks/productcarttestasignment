//
//  ViewController.swift
//  productCartTestAssignment
//
//  Created by Serhii on 4/21/17.
//  Copyright © 2017 Serhii. All rights reserved.
//

import UIKit

protocol ReusableVC: class {

}

/// return String identifier from self name
extension ReusableVC where Self: UIViewController {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
