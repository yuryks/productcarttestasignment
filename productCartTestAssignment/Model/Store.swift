//
//  PCStore.swift
//  productCartTestAssignment
//
//  Created by Serhii on 4/19/17.
//  Copyright © 2017 Serhii. All rights reserved.
//

import Foundation

final class PCStore: PCListable {
    static var shared = PCStore()
    var productList: [ProductObject]
    
    init(productList: [ProductObject] = [ProductObject]()) {
        self.productList = productList
    }
}
