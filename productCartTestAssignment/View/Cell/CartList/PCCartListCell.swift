//
//  PCCartListCell.swift
//  productCartTestAssignment
//
//  Created by Serhii on 4/20/17.
//  Copyright © 2017 Serhii. All rights reserved.
//

import UIKit

class PCCartListCell: UITableViewCell, ReusableView {
    //MARK: - Outlets
    @IBOutlet weak var productDeleteButton: UIButton!
    @IBOutlet fileprivate weak var productQuantityLabel: UILabel!
    @IBOutlet fileprivate weak var productPriceLabel: UILabel!
    @IBOutlet fileprivate weak var productTitleLabel: UILabel!
    
    //MARK: - Cell life
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(from object: PCProductable) {
        self.productTitleLabel.text = object.name
        self.productPriceLabel.text = "\(object.price) $"
        self.productQuantityLabel.text = "\(object.quantity) PC"
    }
}
