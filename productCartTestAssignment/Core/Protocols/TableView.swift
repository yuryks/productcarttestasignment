//
//  TableView.swift
//  productCartTestAssignment
//
//  Created by Serhii on 4/20/17.
//  Copyright © 2017 Serhii. All rights reserved.
//

import UIKit

/// ReusableView view protocol helps to regiter cell for table view
protocol ReusableView: class {
    func configure()
}

/// optional func configure
extension ReusableView {
    func configure(){}
}

/// return String identifier from self name
extension ReusableView where Self: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
