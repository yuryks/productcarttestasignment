//
//  ProductObject.swift
//  productCartTestAssignment
//
//  Created by Serhii on 4/20/17.
//  Copyright © 2017 Serhii. All rights reserved.
//

import Foundation

class ProductObject: PCProductable, Equatable {
    var name: String
    var price: Int
    var quantity: Int
    init(name: String = "", price: Int = 0, quantity: Int = 0) {
        self.name = name
        self.price = price
        self.quantity = quantity
    }
    
    //MARK: - Equatable
    static func == (lhs: ProductObject, rhs: ProductObject) -> Bool{
        return  lhs.name == rhs.name && lhs.price == rhs.price
    }

}
