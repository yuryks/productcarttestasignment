//
//  PCCartViewController.swift
//  productCartTestAssignment
//
//  Created by Serhii on 4/20/17.
//  Copyright © 2017 Serhii. All rights reserved.
//

import UIKit

class PCCartViewController: UIViewController, ReusableVC {
    //MARK: - Outlets
    @IBOutlet fileprivate weak var cartTableView: UITableView!
    @IBOutlet fileprivate weak var closeButton: UIButton!
    
    //MARK: - Variables
    
    //MARK: - VC Life
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()
    }
    
    //MARK: - VC Helpers
    
    /// VC basic preparation
    fileprivate func configure() {
    }
    
    //MARK: - Selector Helpers
    
    /// Remove product from cart
    ///
    /// - Parameter product: product to remove
    fileprivate func removeFromCart(_ product: ProductObject) {
        let elementExists = PCStore.shared.productList.contains { (element) -> Bool in
            if element == product {
                return true
            } else {
                return false
            }
        }
        
        switch elementExists {
        case true:
            for element in PCStore.shared.productList {
                if element.name == product.name {
                    element.quantity += 1
                }
            }
        case false :
            let newProduct = ProductObject(name: product.name, price: product.price, quantity: 1)
            PCStore.shared.productList.append(newProduct)
        }
    }

    //MARK: - Selectors
    
    /// Selector for delete button in cell
    ///
    /// - Parameter sender: UIButton from cell (pay attention at sender.tag)
    @objc fileprivate func deleteButtonPressed(_ sender: UIButton) {
        if PCCart.shared.productList[sender.tag].quantity > 1 {
            PCCart.shared.productList[sender.tag].quantity -= 1
            
            self.removeFromCart(PCCart.shared.productList[sender.tag])
            
            cartTableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .automatic)
        } else {
            PCCart.shared.productList.remove(at: sender.tag)
            cartTableView.reloadData()
            
            if PCCart.shared.productList.count < 1 {
                self.dismiss(animated: true)
            }
        }
    }

    //MARK: - Actions
    @IBAction fileprivate func closeButtonPresed(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
}

//MARK: - UITableViewDataSource
extension PCCartViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PCCart.shared.productList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PCCartListCell.reuseIdentifier) as! PCCartListCell
        
        cell.configure(from: PCCart.shared.productList[indexPath.row])
        cell.productDeleteButton.tag = indexPath.row
        cell.productDeleteButton.addTarget(self, action: #selector(self.deleteButtonPressed(_:)), for: .touchUpInside)

        return cell
    }
}

//MARK: - UITableViewDelegate
extension PCCartViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
