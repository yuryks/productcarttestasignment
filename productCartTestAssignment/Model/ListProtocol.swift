//
//  ListProtocol.swift
//  productCartTestAssignment
//
//  Created by Serhii on 4/20/17.
//  Copyright © 2017 Serhii. All rights reserved.
//

import Foundation

protocol PCListable: class {
    static var shared: Self { get }
    var productList: [ProductObject] {get set}
}

extension PCListable {
        var productList: [ProductObject] {
        get { return [ProductObject]() }
        set {}
    }
}
