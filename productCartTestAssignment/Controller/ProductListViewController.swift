//
//  ProductListViewController.swift
//  productCartTestAssignment
//
//  Created by Serhii on 4/19/17.
//  Copyright © 2017 Serhii. All rights reserved.
//

import UIKit

class PCProductListViewController: UIViewController, ReusableVC {
    //MARK: - Outlets
    @IBOutlet fileprivate weak var priceBarItem: UIBarButtonItem!
    @IBOutlet fileprivate weak var cartBarItem: UIBarButtonItem!
    @IBOutlet fileprivate weak var productTableView: UITableView!
    
    //MARK: - Variables
    fileprivate var productsInCart = 0 {
        didSet {
            (navigationItem.rightBarButtonItems?.first as! ENMBadgedBarButtonItem).badgeValue = "\(productsInCart)"
        }
    }
    fileprivate var productsPrice = 0 {
        didSet {
            navigationItem.rightBarButtonItems?.last?.title = "\(productsPrice) $"
        }
    }
    
    //MARK: - VC Life
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.reloadDataSource() //TODO: think about better way of implementation
    }
    
    //MARK: - VC Helpers
    
    /// VC basic preparation
    fileprivate func configure() {
        self.createObjects()
        self.createNaigationBarItems()
    }
    
    fileprivate func reloadDataSource() {
        productTableView.reloadData()
        self.productsInCart = PCCart.shared.totalProducts
        self.productsPrice = PCCart.shared.totalPrice
    }
    
    /// Create dummy data
    fileprivate func createObjects() {
       let productArray = [ProductObject(name: "bread", price: 10, quantity: 12),
                        ProductObject(name: "milk", price: 14, quantity: 8),
                        ProductObject(name: "soap", price: 23, quantity: 3),
                        ProductObject(name: "apple", price: 7, quantity: 36),
                        ProductObject(name: "tomato", price: 25, quantity: 30),
                        ProductObject(name: "cucumber", price: 6, quantity: 10),
                        ProductObject(name: "banana", price: 8, quantity: 3),
                        ProductObject(name: "pineapple", price: 9, quantity: 2),
                        ProductObject(name: "pineapplepen", price: 2, quantity: 5),
                        ProductObject(name: "penpineapplepen", price: 4, quantity: 11),
                        ProductObject(name: "iphone4s", price: 200, quantity: 10),
                        ProductObject(name: "iphone5", price: 300, quantity: 41),
                        ProductObject(name: "iphone5c", price: 350, quantity: 10),
                        ProductObject(name: "iphone5s", price: 400, quantity: 10),
                        ProductObject(name: "iphone5se", price: 500, quantity: 10),
                        ProductObject(name: "iphone6", price: 550, quantity: 10),
                        ProductObject(name: "iphone6plus", price: 600, quantity: 10),
                        ProductObject(name: "iphone6s", price: 650, quantity: 10),
                        ProductObject(name: "iphone6splus", price: 750, quantity: 10),
                        ProductObject(name: "iphone7", price: 800, quantity: 10)
        ]
        
        for product in productArray {
            PCStore.shared.productList.append(product)
        }
    }
    
    /// Create navigation bar items
    fileprivate func createNaigationBarItems() {
        var cartBarButton: ENMBadgedBarButtonItem!
        let priceBarButton = UIBarButtonItem()
        priceBarButton.title = "0 $"
        
        let image = UIImage(named: "cart")
        let button = UIButton(type: .custom)
        if image != nil {
            button.frame = CGRect(x: 0.0, y: 0.0, width: 22, height: 22)
        } else {
            button.frame = CGRect.zero;
        }
        
        button.setBackgroundImage(image, for: UIControlState())
        button.addTarget(self,
                         action: #selector(self.cartBarButtonPressed),
                         for: .touchUpInside)
        
        let newBarButton = ENMBadgedBarButtonItem(customView: button, value: "\(productsInCart)")
        cartBarButton = newBarButton
        
        navigationItem.rightBarButtonItems = [cartBarButton, priceBarButton]
    }
    
    //MARK: - Selector Helpers
    
    /// Add new product to cart
    ///
    /// - Parameter product: product to add
    fileprivate func addToCart(_ product: ProductObject) {
        let elementExists = PCCart.shared.productList.contains { (element) -> Bool in
            if element == product {
                return true
            } else {
                return false
            }
        }
        
        switch elementExists {
        case true:
            for element in PCCart.shared.productList {
                if element.name == product.name {
                    element.quantity += 1
//                    return // TODO: should I exit if object found
                }
            }
        case false :
            let newProduct = ProductObject(name: product.name, price: product.price, quantity: 1)
            PCCart.shared.productList.append(newProduct)
        }
        
        self.productsInCart = PCCart.shared.totalProducts
        self.productsPrice = PCCart.shared.totalPrice
    }
    
    //MARK: - Selectors
    
    /// Selector for buy button in cell
    ///
    /// - Parameter sender: UIButton from cell (pay attention at sender.tag)
    @objc fileprivate func buyButtonPressed(_ sender: UIButton) {
        if PCStore.shared.productList[sender.tag].quantity > 1 {
            PCStore.shared.productList[sender.tag].quantity -= 1
            
            self.addToCart(PCStore.shared.productList[sender.tag])
            
            productTableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .automatic)
        } else {
            PCStore.shared.productList.remove(at: sender.tag)
            productTableView.reloadData()
        }
    }

    /// selector for navigation bar cart button
    @objc fileprivate func cartBarButtonPressed() {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: PCCartViewController.reuseIdentifier)
        self.present(vc, animated: true, completion: nil)
    }
}

//MARK: - UITableViewDataSource
extension PCProductListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PCStore.shared.productList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PCProductListCell.reuseIdentifier) as! PCProductListCell
        
        cell.configure(from: PCStore.shared.productList[indexPath.row])
        cell.productBuyButton.tag = indexPath.row
        cell.productBuyButton.addTarget(self, action: #selector(self.buyButtonPressed(_:)), for: .touchUpInside)
        
        return cell
    }
}

//MARK: - UITableViewDelegate
extension PCProductListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

